/******************************************************************
 *   © 2020 Matteo Bigliardi
 *   email: 239958@studenti.unimore.it
 * 
 *   mbnodecontrollerifopt.cpp
 * 
 ******************************************************************/


#include <ifopt/problem.h>
#include <ifopt/ipopt_solver.h>
#include <ifopt/variable_set.h>
#include <ifopt/constraint_set.h>
#include <ifopt/cost_term.h>
#include <Eigen/Dense>
#include <iostream>
#include <fstream>
#include <chrono>

#include <stdint.h> 
#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>      
#include <time.h> 


#include <ros/ros.h>
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"

#include "rosgraph_msgs/Clock.h"
#include "geometry_msgs/TransformStamped.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Pose2D.h"
#include "nav_msgs/Path.h"




// variabili per memorizzare traiettoria in memoria
int flagMemorizzaTraiettoria =0;
const int maxElem = 5000;
std::vector<double> MemX(maxElem);
std::vector<double> MemY(maxElem);
int Elem = 0;
std::vector<double> TargetFinale (3);

// file per traiettoria
std::ofstream fTraiettoria;

// file tempi di ottimizzazione
std::ofstream FtempiOttimizzazione;

// file tempi di CPU
std::ofstream FtempiCPU;

// file durata percorso
int ho_iniziato = 0;
std::ofstream FdurataPercorso;
auto inizioPercorso = std::chrono::steady_clock::now();
auto finePercorso = std::chrono::steady_clock::now();

// file distanza percorsa
std::ofstream FdistanzaPercorsa;
double G_distanza_percorsa = 0;



// ostacoli
double ro= 0.12;
std::vector <double> xo (4);
std::vector <double> yo (4);

// flag  se in trajectory following flagTF=1
int flagTF =0;

// primo_target
bool primo_target=true;

// flag inizio ottimizzazione
int flagOpt=0;

//----DatiPosizione--------------------------------------
Eigen::VectorXd DPxk(2700);
int ScorriDPxk=0;
Eigen::VectorXd tempxk(2700);

//-----VarGlobali---------------------------------------------
double pi = 3.14159265358;

std::vector<double> G_posizione_iniziale (3);

std::vector<double> G_vel_precedente {0,0};

double G_C1, G_C2, G_C3, G_C4, G_C5, G_C6, G_C7, G_C8 =0;
std::vector<double> G_C_h {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};



// implementazione funzioni ifopt
namespace ifopt
{
  using Eigen::VectorXd;

  class ExVariables : public VariableSet
  {
  public:
    // ogni set di variabili ha un nome ("var_set1") per permettere a vincoli e costi
    // di definire valori e jacobiane in modo specifico per il singolo set
    ExVariables() : ExVariables("var_set1"){};
    ExVariables(const std::string &name) : VariableSet(6, name)
    {
      // i valori iniziali dove l'algoritmo parte ad iterare
      du11 = 0.2;
      du12 =-0.2;
      du21 = 0;
      du22 = 0;
      du31 = 0;
      du32 = 0;
    }

    // qui si trasforma Eigen::Vector in una qualsiasi rappresentazioneHere is where you can transform the Eigen::Vector into whatever
    // delle proprie variabili, anche classi complesse
    void SetVariables(const VectorXd &du) override
    {
      du11 = du(0);
      du12 = du(1);
      du21 = du(2);
      du22 = du(3);
      du31 = du(4);
      du32 = du(5);
    };

    // qui vi è la trasformazione opposta
    VectorXd GetValues() const override
    {
      Eigen::VectorXd wXd(6);
      wXd << du11, du12,
             du21, du22,
             du31, du32;
      return wXd;
    };

    // introduzione limiti superiori ed inferiori delle variabili
    VecBound GetBounds() const override
    {
      VecBound bounds(GetRows());
      bounds.at(0) = Bounds(-0.2, 0.2);
      bounds.at(1) = Bounds(-0.2, 0.2);
      bounds.at(2) = Bounds(-0.2, 0.2);
      bounds.at(3) = Bounds(-0.2, 0.2);
      bounds.at(4) = Bounds(-0.2, 0.2);
      bounds.at(5) = Bounds(-0.2, 0.2);
      return bounds;
    }

  private:
   
    double du11, du12,
           du21, du22,
           du31, du32;
  };

   // inizio funzione di costo
   // -----mobRobCT0-----------------------------------------------------
   Eigen::VectorXd mobRobCT0(Eigen::VectorXd x, Eigen::VectorXd u)
    {
      // modello lineare tempo-continuo di un differential drive robot
      // 3 stati (x):
      // posizione del robot lungo x (x)
      // posizione del robot lungo y (y)
      // angolo di sterzo del robot (theta)
      // 2 inputs (u):
      // velocità lineare (v_r)
      // velocità angolare (w_r)
      double theta_r = x(2);
      Eigen::VectorXd dxdt = x;
      double v_r = u(0);
      double w_r = u(1);
      dxdt(0) = cos(theta_r) * v_r;
      dxdt(1) = sin(theta_r) * v_r;
      dxdt(2) = w_r;
      Eigen::VectorXd y = x;

      return dxdt;
    };
    
    //-------wrapToPi-----------------------------------------------------
    double wrapToPi(double x)
    {
      x = fmod(x + pi,2*pi);
      if (x < 0)
          x += 2*pi;
      
      return x - pi;
    };

    // -----mobRobDT0-----------------------------------------------------
    Eigen::VectorXd mobRobDT0(Eigen::VectorXd xk, Eigen::VectorXd uk, double Ts)
    {
      int M = 10;
      double delta = Ts / M;
      Eigen::VectorXd xk1 = xk;

      for (int i = 0; i < M; i++)
      {
        xk1 = xk1 + delta*mobRobCT0(xk1,uk); 
        
      }

      // chiamata wrapToPi
      xk1(2)= wrapToPi(xk1(2));
      Eigen::VectorXd yk = xk;

      return xk1;
    };

    // -------mobRobCostFCN-----------------------------------------------
    double mobRobCostFCN(Eigen::VectorXd du, Eigen::VectorXd x0, double Ts, int N, int M, Eigen::MatrixXd xref, Eigen::VectorXd u1)
    {
      /* funzione di costo di NMPC per il controllo di robot mobili

      Inputs:
      du:     variabili di ottimizzazione, dal tempo k al tempo k+M-1 come vettore colonna
      x:      stato corrente al tempo k
      Ts:     tempo di campionamento del controllore
      N:      orizzonte di predizione
      M:      orizzonte di controllo
      xref:   stati del riferimento come una matrice le cui N+1 colonne ( dal tempo 0 al tempo N ) sono i riferimenti per il sistema
      u1:     precedente output del controllore al tempo k-1
      J:      costo della funzione oggetto

      */

      // la matrice Q penalizza le deviazioni dello stato dal riferimento
      Eigen::DiagonalMatrix<double, 3> Q(8, 10, 0.1);
      
      // matrice del costo terminale
      Eigen::DiagonalMatrix<double, 3> P = 100 * Q;

      // la matrice Rdu penalizza il rateo di cambiamento dell'input (du)
      Eigen::DiagonalMatrix<double, 2> Rdu(8, 8);

      // la amtrice Ru penalizza la grandezza dell'input (u)
      Eigen::DiagonalMatrix<double, 2> Ru(8, 8);

      // inizializzazione ref_k
      Eigen::VectorXd ref_k(3);
      ref_k << 0, 0, 0;

      // inizializzazione theta_d
      double theta_d = 0.0;

      // inizializzazione ek
      Eigen::VectorXd ek(3);
      ek << 0, 0, 0;

      // trasformazione di du in una matrice le cui righe sono du[k] da k a k+N, 
      // riempendo le righe dopo M con zero
      Eigen::MatrixXd alldu(N, 2);
      alldu.setZero(N, 2);
      int w = 0;
      for (int i = 0; i < M; ++i)
      {
        for (int c = 0; c < 2; ++c)
        {
          alldu(i, c) = du(w);
          w++;
        }
      }

      // calcolo del costo
      // inserimento stato iniziale e costo

      //xk = x0;
      Eigen::VectorXd xk(3);
      xk << 0, 0, 0;
      for (int i = 0; i < 3; i++)
      {
        xk(i) = x0(i);
      }

      //uk = u1;
      Eigen::VectorXd uk(2);
      uk << 0, 0;
      for (int i = 0; i < 2; i++)
      {
        uk(i) = u1(i);
      }

      //init J
      double j = 0;
      
      //costo primi N istanti
      for (int i = 0; i < N; i++)
      {
        Eigen::VectorXd du_k(2);
        du_k << 0, 0;
        for (int j = 0; j < 2; j++)
        {
          du_k(j) = alldu(i, j);
        }
        du_k = du_k.transpose();
        uk = uk + du_k;

        for (int j = 0; j < 3; j++)
        {
          ref_k(j) = xref(i, j);
        }
        ref_k = ref_k.transpose();
        theta_d = ref_k(2);

        //Rot
        Eigen::MatrixXd Rot(3, 3);
        Rot.setZero(3, 3);
        Rot << cos(theta_d), sin(theta_d), 0, (-sin(theta_d)), cos(theta_d), 0, 0, 0, 1;

        // calcolo ek
        ek = Rot * (xk - ref_k);
        
        // calcolo costo
        j = j + ek.transpose() * Q * ek;
        j = j + du_k.transpose() * Rdu * du_k;
        j = j + uk.transpose() * Ru * uk;

        // ottengo stato al prossimo istante di predizione
        xk = mobRobDT0(xk, uk, Ts);
      }

      // costo terminale
      for (int j = 0; j < 3; j++)
      {
        ref_k(j) = xref(N, j);
      }

      ref_k = ref_k.transpose();

      theta_d = ref_k(2);

      Eigen::MatrixXd Rot(3, 3);
      Rot.setZero(3, 3);
      Rot << cos(theta_d), sin(theta_d), 0, (-sin(theta_d)), cos(theta_d), 0, 0, 0, 1;
      ek = Rot * (xk - ref_k);

      j = j + ek.transpose() * P * ek;
      // std::cout << "J = " <<std::to_string(j) << std::endl;

      return j;
    };

    double FunzioneDiCosto(Eigen::VectorXd du)
    {
      int M = 3;
      int N = 15;

      Eigen::VectorXd x0(3);
      x0<<G_posizione_iniziale[0],G_posizione_iniziale[1],G_posizione_iniziale[2];
    
      double Ts = 0.1;

      Eigen::MatrixXd xref(N + 1, 3);
      xref.setZero(N + 1, 3);

      // valorizzo il riferimento
      for (int i = 0; i < N+1; ++i)
      {
        for (int c = 0; c < 3; ++c)
        {
          if(ScorriDPxk+c+3*i<=(DPxk.size()-4)){
            xref(i,c)=DPxk(ScorriDPxk+c+3*i);
          } else {
            xref(i,c)=DPxk((DPxk.size()-3)+c);
          }
        }
      }
      
      Eigen::VectorXd u1(2);
      u1<<G_vel_precedente[0],G_vel_precedente[1];

      // ottengo J
      double j = mobRobCostFCN(du, x0, Ts, N, M, xref, u1);
      return j;
    };
    // fine funzione di costo


  class ExConstraint : public ConstraintSet
  {
  public:
    ExConstraint() : ExConstraint("constraint1") {}
    ExConstraint(const std::string &name) : ConstraintSet(10, name) {}
    // ottengo vincoli
    VectorXd GetValues() const override
    {
      VectorXd g(GetRows());

      VectorXd du = GetVariables()->GetComponent("var_set1")->GetValues();

      // matrice per calcolo wr wl
      Eigen::MatrixXd M(6, 6);
      M.setZero(6, 6);
      M(0,0)=1/r;
      M(0,1)=d/r;
      M(1,0)=1/r;
      M(1,1)=-d/r;
      M(2,2)=1/r;
      M(2,3)=d/r;
      M(3,2)=1/r;
      M(3,3)=-d/r;
      M(4,4)=1/r;
      M(4,5)=d/r;
      M(5,4)=1/r;
      M(5,5)=-d/r;

      // calcolo velocità istanti successivi
      double v1= G_vel_precedente[0]+du(0);
      double w1= G_vel_precedente[1]+du(1);
      double v2= G_vel_precedente[0]+du(0)+du(2);
      double w2= G_vel_precedente[1]+du(1)+du(3);
      double v3= G_vel_precedente[0]+du(0)+du(2)+du(4);
      double w3= G_vel_precedente[1]+du(1)+du(3)+du(5);
      Eigen::VectorXd u(6);
      u<<v1,w1,v2,w2,v3,w3;

      // calcolo wr wl istanti successivi
      Eigen::VectorXd temp(6);
      temp=M*u;

      // inserisco wr e wl nel vettore dei vincoli
      for (int i = 0; i < 6; i++)
      {
        g(i)=temp(i);
      }
      
      // inizio calcolo per la previsione delle posizioni successive
      Eigen::VectorXd xin(3);
      xin<<G_posizione_iniziale[0],G_posizione_iniziale[1],G_posizione_iniziale[2];
      double T=0.1;
      
      Eigen::VectorXd uk1(2);
      uk1<<v1, w1;
      Eigen::VectorXd uk2(2);
      uk2<<v2, w2;
      Eigen::VectorXd uk3(2);
      uk3<<v3, w3;

      Eigen::VectorXd xk1(3);
      xk1 = mobRobDT0(xin, uk1, T);
      Eigen::VectorXd xk2(3);
      xk2 = mobRobDT0(xk1, uk2, T);
      Eigen::VectorXd xk3(3);
      xk3 = mobRobDT0(xk2, uk3, T);
      Eigen::VectorXd xk4(3);
      xk4 = mobRobDT0(xk3, uk3, T);
      Eigen::VectorXd xk5(3);
      xk5 = mobRobDT0(xk4, uk3, T);
      Eigen::VectorXd xk6(3);
      xk6 = mobRobDT0(xk5, uk3, T);
      Eigen::VectorXd xk7(3);
      xk7 = mobRobDT0(xk6, uk3, T);
      Eigen::VectorXd xk8(3);
      xk8 = mobRobDT0(xk7, uk3, T);
      Eigen::VectorXd xk9(3);
      xk9 = mobRobDT0(xk8, uk3, T);
      Eigen::VectorXd xk10(3);
      xk10 = mobRobDT0(xk9, uk3, T);
      Eigen::VectorXd xk11(3);
      xk11 = mobRobDT0(xk10, uk3, T);
      Eigen::VectorXd xk12(3);
      xk12 = mobRobDT0(xk11, uk3, T);
    
      double x12= xk12(0);
      double y12= xk12(1);
      
      // ostacoli scena 1
      xo= {0.40, 0.85, 1.10, 1.20};
      yo= {0.30, 0.65, 0.82, 0.55};
      // ostacoli scena 2
      // xo= {0.50, 1.00, 1.00, 1.00};
      // yo= {0.50, 0.50, 0.25, 0.75};
     
      // distanza dagli ostacoli
      double C1= sqrt((y12-yo[0])*(y12-yo[0])+(x12-xo[0])*(x12-xo[0]));
      double C2= sqrt((y12-yo[1])*(y12-yo[1])+(x12-xo[1])*(x12-xo[1]));
      double C3= sqrt((y12-yo[2])*(y12-yo[2])+(x12-xo[2])*(x12-xo[2]));
      double C4= sqrt((y12-yo[3])*(y12-yo[3])+(x12-xo[3])*(x12-xo[3]));
      G_C1= C1;
      G_C2= C2;
      G_C3= C3;
      G_C4= C4;
      
      //inizio calcolo per trovare derivata approssimata
      G_C_h.clear();
      for (int j = 0; j < 6; j++)
      {
        du(j)=du(j)+0.001;

        double v1_h= G_vel_precedente[0]+du(0);
        double w1_h= G_vel_precedente[1]+du(1);
        double v2_h= G_vel_precedente[0]+du(0)+du(2);
        double w2_h= G_vel_precedente[1]+du(1)+du(3);
        double v3_h= G_vel_precedente[0]+du(0)+du(2)+du(4);
        double w3_h= G_vel_precedente[1]+du(1)+du(3)+du(5);

        Eigen::VectorXd uk1_h(2);
        uk1_h<<v1_h, w1_h;
        Eigen::VectorXd uk2_h(2);
        uk2_h<<v2_h, w2_h;
        Eigen::VectorXd uk3_h(2);
        uk3_h<<v3_h, w3_h;

        Eigen::VectorXd xk1_h(3);
        xk1_h = mobRobDT0(xin, uk1_h, T);
        Eigen::VectorXd xk2_h(3);
        xk2_h = mobRobDT0(xk1_h, uk2_h, T);
        Eigen::VectorXd xk3_h(3);
        xk3_h = mobRobDT0(xk2_h, uk3_h, T);
        Eigen::VectorXd xk4_h(3);
        xk4_h = mobRobDT0(xk3_h, uk3_h, T);
        Eigen::VectorXd xk5_h(3);
        xk5_h = mobRobDT0(xk4_h, uk3_h, T);
        Eigen::VectorXd xk6_h(3);
        xk6_h = mobRobDT0(xk5_h, uk3_h, T);
        Eigen::VectorXd xk7_h(3);
        xk7_h = mobRobDT0(xk6_h, uk3_h, T);
        Eigen::VectorXd xk8_h(3);
        xk8_h = mobRobDT0(xk7_h, uk3_h, T);
        Eigen::VectorXd xk9_h(3);
        xk9_h = mobRobDT0(xk8_h, uk3_h, T);
        Eigen::VectorXd xk10_h(3);
        xk10_h = mobRobDT0(xk9_h, uk3_h, T);
        Eigen::VectorXd xk11_h(3);
        xk11_h = mobRobDT0(xk10_h, uk3_h, T);
        Eigen::VectorXd xk12_h(3);
        xk12_h = mobRobDT0(xk11_h, uk3_h, T);
        
        double x12_h= xk12_h(0);
        double y12_h= xk12_h(1);

        double C1_h= sqrt((y12_h-yo[0])*(y12_h-yo[0])+(x12_h-xo[0])*(x12_h-xo[0]));
        double C2_h= sqrt((y12_h-yo[1])*(y12_h-yo[1])+(x12_h-xo[1])*(x12_h-xo[1]));
        double C3_h= sqrt((y12_h-yo[2])*(y12_h-yo[2])+(x12_h-xo[2])*(x12_h-xo[2]));
        double C4_h= sqrt((y12_h-yo[3])*(y12_h-yo[3])+(x12_h-xo[3])*(x12_h-xo[3]));

        du(j)=du(j)-0.001;

        G_C_h.push_back(C1_h);
        G_C_h.push_back(C2_h);
        G_C_h.push_back(C3_h);
        G_C_h.push_back(C4_h);
      }
      
      Eigen::VectorXd temp1(4);
      temp1<<C1,C2,C3,C4; 
      
      //inserisco distanza dagli ostacoli nel vettore dei vincoli 
      for (int i = 6; i < 10; i++)
      {
        g(i)=temp1(i-6);
      }
      
      return g;
    };
    
    // inserimento vincoli di disuguaglianza
    VecBound GetBounds() const override
    { 
      VecBound b(GetRows());

      // vincoli di velocità
      b.at(0) = Bounds(-1.5*pi,1.5* pi);
      b.at(1) = Bounds(-1.5*pi,1.5* pi);
      b.at(2) = Bounds(-1.5*pi,1.5* pi);
      b.at(3) = Bounds(-1.5*pi,1.5* pi);
      b.at(4) = Bounds(-1.5*pi,1.5* pi);
      b.at(5) = Bounds(-1.5*pi, 1.5*pi);

      // vincoli di posizione
      b.at(6) = Bounds(ro, inf);
      b.at(7) = Bounds(ro, inf);
      b.at(8) = Bounds(ro, inf);
      b.at(9) = Bounds(ro, inf);
    
      return b;
    }

    // questa funzione fornisce la derivata prima dei vincoli
    void FillJacobianBlock(std::string var_set, Jacobian &jac_block) const override
    {
      if (var_set == "var_set1")
      {
        VectorXd du = GetVariables()->GetComponent("var_set1")->GetValues();

        // velocita' 
        jac_block.coeffRef(0, 0) = 1/r;
        jac_block.coeffRef(0, 1) = d/r;
        jac_block.coeffRef(0, 2) = 0.0;
        jac_block.coeffRef(0, 3) = 0.0;
        jac_block.coeffRef(0, 4) = 0.0;
        jac_block.coeffRef(0, 5) = 0.0;
        
        jac_block.coeffRef(1, 0) = 1/r;
        jac_block.coeffRef(1, 1) = -d/r;
        jac_block.coeffRef(1, 2) = 0.0;
        jac_block.coeffRef(1, 3) = 0.0;
        jac_block.coeffRef(1, 4) = 0.0;
        jac_block.coeffRef(1, 5) = 0.0;

        jac_block.coeffRef(2, 0) = 1/r;
        jac_block.coeffRef(2, 1) = d/r;
        jac_block.coeffRef(2, 2) = 1/r;
        jac_block.coeffRef(2, 3) = d/r;
        jac_block.coeffRef(2, 4) = 0.0;
        jac_block.coeffRef(2, 5) = 0.0;

        jac_block.coeffRef(3, 0) = 1/r;
        jac_block.coeffRef(3, 1) = -d/r;
        jac_block.coeffRef(3, 2) = 1/r;
        jac_block.coeffRef(3, 3) = -d/r;
        jac_block.coeffRef(3, 4) = 0.0;
        jac_block.coeffRef(3, 5) = 0.0;

        jac_block.coeffRef(4, 0) = 1/r;
        jac_block.coeffRef(4, 1) = d/r;
        jac_block.coeffRef(4, 2) = 1/r;
        jac_block.coeffRef(4, 3) = d/r;
        jac_block.coeffRef(4, 4) = 1/r;
        jac_block.coeffRef(4, 5) = d/r;

        jac_block.coeffRef(5, 0) = 1/r;
        jac_block.coeffRef(5, 1) = -d/r;
        jac_block.coeffRef(5, 2) = 1/r;
        jac_block.coeffRef(5, 3) = -d/r;
        jac_block.coeffRef(5, 4) = 1/r;
        jac_block.coeffRef(5, 5) = -d/r;

        // posizione
        double h=0.001;

        jac_block.coeffRef(6, 0) = (G_C_h[0]-G_C1)/h;
        jac_block.coeffRef(6, 1) = (G_C_h[4]-G_C1)/h;
        jac_block.coeffRef(6, 2) = (G_C_h[8]-G_C1)/h;
        jac_block.coeffRef(6, 3) = (G_C_h[12]-G_C1)/h;
        jac_block.coeffRef(6, 4) = (G_C_h[16]-G_C1)/h;
        jac_block.coeffRef(6, 5) = (G_C_h[20]-G_C1)/h;

        jac_block.coeffRef(7, 0) = (G_C_h[1]-G_C2)/h;
        jac_block.coeffRef(7, 1) = (G_C_h[5]-G_C2)/h;
        jac_block.coeffRef(7, 2) = (G_C_h[9]-G_C2)/h;
        jac_block.coeffRef(7, 3) = (G_C_h[13]-G_C2)/h;
        jac_block.coeffRef(7, 4) = (G_C_h[17]-G_C2)/h;
        jac_block.coeffRef(7, 5) = (G_C_h[21]-G_C2)/h;

        jac_block.coeffRef(8, 0) = (G_C_h[2]-G_C3)/h;
        jac_block.coeffRef(8, 1) = (G_C_h[6]-G_C3)/h;
        jac_block.coeffRef(8, 2) = (G_C_h[10]-G_C3)/h;
        jac_block.coeffRef(8, 3) = (G_C_h[14]-G_C3)/h;
        jac_block.coeffRef(8, 4) = (G_C_h[18]-G_C3)/h;
        jac_block.coeffRef(8, 5) = (G_C_h[22]-G_C3)/h;

        jac_block.coeffRef(9, 0) = (G_C_h[3]-G_C4)/h;
        jac_block.coeffRef(9, 1) = (G_C_h[7]-G_C4)/h;
        jac_block.coeffRef(9, 2) = (G_C_h[11]-G_C4)/h;
        jac_block.coeffRef(9, 3) = (G_C_h[15]-G_C4)/h;
        jac_block.coeffRef(9, 4) = (G_C_h[19]-G_C4)/h;
        jac_block.coeffRef(9, 5) = (G_C_h[23]-G_C4)/h;
      }

    }
    private:
      double r= 0.020;
      double d= 0.053/2;  
  };

  class ExCost : public CostTerm
  {
  public:
    ExCost() : ExCost("cost_term1") {}
    ExCost(const std::string &name) : CostTerm(name) {}

    double GetCost() const override
    {
      
      VectorXd du = GetVariables()->GetComponent("var_set1")->GetValues();
      double j = FunzioneDiCosto(du);
      return j;
    };

    void FillJacobianBlock(std::string var_set, Jacobian &jac) const override
    {
      if (var_set == "var_set1")
      {
        VectorXd du = GetVariables()->GetComponent("var_set1")->GetValues();

        Eigen::VectorXd h(6);
        h << 0.001, 0.001, 0.001, 0.001, 0.001, 0.001; 
        
        double FC = FunzioneDiCosto(du);
         
        // derivata approssimata rispetto a du(0)  
        du(0)= du(0) + h(0);
        double FC_h = FunzioneDiCosto(du);
        double derivata_approssimata_u11= (FC_h-FC)/0.001;
        du(0)= du(0) - h(0);
        // derivata approssimata rispetto a du(1)  
        du(1)= du(1) + h(1);
        FC_h = FunzioneDiCosto(du);
        double derivata_approssimata_u12= (FC_h-FC)/0.001;
        du(1)= du(1) - h(1);
        // derivata approssimata rispetto a du(2)  
        du(2)= du(2) + h(2);
        FC_h = FunzioneDiCosto(du);
        double derivata_approssimata_u21= (FC_h-FC)/0.001;
        du(2)= du(2) - h(2);
        // derivata approssimata rispetto a du(3)  
        du(3)= du(3) + h(3);
        FC_h = FunzioneDiCosto(du);
        double derivata_approssimata_u22= (FC_h-FC)/0.001;
        du(3)= du(3) - h(3);
        // derivata approssimata rispetto a du(4)  
        du(4)= du(4) + h(4);
        FC_h = FunzioneDiCosto(du);
        double derivata_approssimata_u31= (FC_h-FC)/0.001;
        du(4)= du(4) - h(4);
        // derivata approssimata rispetto a du(5)  
        du(5)= du(5) + h(5);
        FC_h = FunzioneDiCosto(du);
        du(5)= du(5) - h(5);
        double derivata_approssimata_u32= (FC_h-FC)/0.001;

        jac.coeffRef(0, 0) = derivata_approssimata_u11;
        jac.coeffRef(0, 1) = derivata_approssimata_u12;
        jac.coeffRef(0, 2) = derivata_approssimata_u21;
        jac.coeffRef(0, 3) = derivata_approssimata_u22;
        jac.coeffRef(0, 4) = derivata_approssimata_u31;
        jac.coeffRef(0, 5) = derivata_approssimata_u32;
  
      }
    };

  };

} // namespace ifopt


// class controller
class Controller
{
public:
  Controller(ros::NodeHandle &nh);

  // LABORATORIO void ePuck_poseCallbackTEST(const geometry_msgs::PoseStamped trans);
  void ePuck_poseCallbackTEST(const geometry_msgs::TransformStamped trans);
  void clockCallbackTEST(const rosgraph_msgs::Clock rclock);
  void timer1CallbackTEST(const ros::TimerEvent &);
  void targetCallback(const geometry_msgs::Pose2D target );
  void trajectoryCallback(const nav_msgs::Path trajectory);
  
protected:
  ros::Publisher commandPub;
  ros::Time robot_clock; 
};
 
Controller::Controller(ros::NodeHandle &nh)
{
  // LABORATORIO commandPub = nh.advertise<geometry_msgs::Twist>("/epuck_robot_4/mobile_base/cmd_vel", 10);
  commandPub = nh.advertise<geometry_msgs::Twist>("/ePuck2/cmd_vel", 10);
  ROS_INFO("..creata istanza controller");
};

void Controller::timer1CallbackTEST(const ros::TimerEvent &)
{
  geometry_msgs::Twist msg;

  // definizione problema
  ifopt::Problem nlp;
  nlp.AddVariableSet  (std::make_shared<ifopt::ExVariables>());
  nlp.AddConstraintSet(std::make_shared<ifopt::ExConstraint>());
  nlp.AddCostSet      (std::make_shared<ifopt::ExCost>());

  // scelta opzioni di ottimizzazione
  ifopt::IpoptSolver ipopt;
  ipopt.SetOption("linear_solver", "mumps");
  ipopt.SetOption("jacobian_approximation", "exact");
  ipopt.SetOption("max_iter", 3000);
  ipopt.SetOption("print_level", 0);
  // ipopt.SetOption("tol", 0.0001);
  
  // incremento del contatore per il vector DPxk, inserito qui per seguire il looprate
  if(ScorriDPxk<((DPxk.size())-3)){
        ScorriDPxk=ScorriDPxk+3;
      }
  // std::cout<<"scorridpxk="<<std::to_string((ScorriDPxk))<<std::endl;

  // aggiungo raggiungimento della posizione iniziale prima del trajectory following
  double dist_pos_in = sqrt((DPxk(ScorriDPxk)-G_posizione_iniziale[0])*(DPxk(ScorriDPxk)-G_posizione_iniziale[0])+(DPxk(ScorriDPxk+1)-G_posizione_iniziale[1])*(DPxk(ScorriDPxk+1)-G_posizione_iniziale[1]));
  if(flagTF==1){
    if(dist_pos_in<=0.10){
      ScorriDPxk=0;
      DPxk=tempxk;
      flagTF=0;
    }
  }
  
  // flag partenza ottimizzazione
  if (flagOpt == 1)
  {
    if (ho_iniziato == 0)   {
       ho_iniziato = 1;
       inizioPercorso = std::chrono::steady_clock::now();
    }

    // inizio misura tempo di ottimizzazione
    auto start = std::chrono::steady_clock::now();
    // inizio misura utilizzo CPU
    clock_t ct1, ct2;  
    if ((ct1 = clock ()) == -1)  
        perror ("clock");

    // risoluzione
    ipopt.Solve(nlp);

    // fine misura utilizzo CPU
    if ((ct2 = clock ()) == -1)      
        perror ("clock");
    FtempiCPU << ct2 - ct1 << "\n";

    // fine misura tempo di ottimizzazione
    auto end = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    FtempiOttimizzazione << elapsed_seconds.count() << "\n";
    
    //std::cout<<std::to_string(ipopt.GetReturnStatus())<<std::endl;
    //intercetto fallimento ottimizzatore
    if (ipopt.GetReturnStatus() == 2 || ipopt.GetReturnStatus() == -2 || ipopt.GetReturnStatus() == -13)
    {
      msg.linear.x = 0;
      msg.angular.z = 0;
      G_vel_precedente[0] = msg.linear.x;
      G_vel_precedente[1] = msg.angular.z;
    }
    else
    {
      Eigen::VectorXd opt_values = nlp.GetOptVariables()->GetValues();
      //std::cout<<opt_values(1)<<std::endl;
      //std::cout<<opt_values(0)<<std::endl;
      msg.linear.x = G_vel_precedente[0] + opt_values(0);
      msg.angular.z = G_vel_precedente[1] + opt_values(1);
      G_vel_precedente[0] = msg.linear.x;
      G_vel_precedente[1] = msg.angular.z;
    }
  }
  else if (flagOpt == 0)
  {
    msg.linear.x = 0;
    msg.angular.z = 0;
    G_vel_precedente[0] = msg.linear.x;
    G_vel_precedente[1] = msg.angular.z;
  }

  // memorizzo traiettoria in memoria
  if (flagMemorizzaTraiettoria==1){
    MemX[Elem]=G_posizione_iniziale[0];
    MemY[Elem]=G_posizione_iniziale[1];
    double deltaAng = abs(G_posizione_iniziale[2]-TargetFinale[2]);
    double delta = sqrt((G_posizione_iniziale[0]-TargetFinale[0])*(G_posizione_iniziale[0]-TargetFinale[0])+(G_posizione_iniziale[1]-TargetFinale[1])*(G_posizione_iniziale[1]-TargetFinale[1]));
    // std::cout<<"delta = "<<std::to_string(delta)<<" deltaAng = "<<std::to_string(deltaAng)<<std::endl;
    if(delta <= 0.10 && deltaAng <= 0.05){
       finePercorso  = std::chrono::steady_clock::now();   
       std::chrono::duration<double> durata_percorso_in_secondi = finePercorso - inizioPercorso;
       FdurataPercorso << durata_percorso_in_secondi.count() << "\n";
       FdistanzaPercorsa << std::to_string(G_distanza_percorsa) << "\n";
       fTraiettoria.open ("OUT_ifopt_traiettoria.csv");
       flagMemorizzaTraiettoria=0;
       int k = 0;
       for (k=0; k<=Elem; k++){
          fTraiettoria << MemX[k] << ";" << MemY[k] << "\n";
          std::cout<<">>>>>>>>>>>>> SCRIVO RIGA TRAIETTORIA!!!!"<<std::endl;
       }
       fTraiettoria.close();
    }
    Elem = Elem+1;
  }
  // pubblicazione comandi di velocità
  commandPub.publish(msg);

}

void Controller::ePuck_poseCallbackTEST(const geometry_msgs::TransformStamped trans)
{
  // LABORATORIO
  // double x = trans.pose.position.x;
  // double y = trans.pose.position.y;
  // double z = trans.pose.orientation.z;
  double x = trans.transform.translation.x;
  double y = trans.transform.translation.y;
  double z = trans.transform.rotation.z;
  double w = trans.transform.rotation.w;
  // std::cout<<"w="<<std::to_string(w)<<std::endl;
  double teta=0;
  
  teta = (2*asin(z));
  if(teta>-pi/2 && teta< pi/2){
    teta= -teta;
  }else if (teta > pi/2 && teta <pi && w>0){
    teta=teta;
  }else if (teta >= pi/2 && teta <pi && w<0){
    teta= -teta;
  }
  //std::cout<<"x="<<std::to_string(x)<<" "<<"y="<<std::to_string(y)<<" "<<"teta="<<std::to_string(teta)<<std::endl;
  
   if(ho_iniziato==1 && flagMemorizzaTraiettoria==1 ){
     double tmp = sqrt((G_posizione_iniziale[0]-x)*(G_posizione_iniziale[0]-x)+(G_posizione_iniziale[1]-y)*(G_posizione_iniziale[1]-y));
     G_distanza_percorsa= G_distanza_percorsa+tmp;
   }

  double rx = ((rand() % 6)-3)/100;
  double ry = ((rand() % 6)-3)/100;
  double rteta = ((rand() % 6)-3)/100;
  x= x + rx;
  y= y + ry;
  teta= teta + rteta;

  // passaggio posizione a vector globale
  std::vector<double> v_pose ={x,y,teta};
  G_posizione_iniziale = v_pose;
}

// intercettazione posizione desiderata
void Controller::targetCallback(const geometry_msgs::Pose2D target){ 
  ScorriDPxk=0;

  double x = target.x;
  double y = target.y;
  double teta = target.theta;
  
  teta = fmod(teta + pi,2*pi);
  if (teta < 0){
    teta += 2*pi;
  }
  teta= teta-pi;

  DPxk.resize(2700);

  for (int k =0 ; k <= 2697; k=k+3)
  {
    
    DPxk(k) = x;
    DPxk(k+1) = y;
    DPxk(k+2) = teta;
  }
  flagOpt = 1;
  TargetFinale={x, y, teta};
  flagMemorizzaTraiettoria=1;
 
  
}

// intercettazione traiettoria desiderata
void Controller::trajectoryCallback(const nav_msgs::Path trajectory){
  ScorriDPxk=0;

  double n_punti_traiettoria=trajectory.poses.size();
  
  DPxk.resize(n_punti_traiettoria*3);
  tempxk.resize(n_punti_traiettoria*3);

  double i=0;
  for(auto pose : trajectory.poses){
     double x = pose.pose.position.x;
     double y = pose.pose.position.y;
     double qz = pose.pose.orientation.z;
     double teta = -(2*asin(qz));
     tempxk(i)=x;
     tempxk(i+1)=y;
     tempxk(i+2)=teta;
     DPxk(i)=tempxk(0);
     DPxk(i+1)=tempxk(1);
     DPxk(i+2)=tempxk(2);
     i= i+3;
  }
  double xtf= tempxk(tempxk.size()-3);
  double ytf= tempxk(tempxk.size()-2);
  double tetatf= tempxk(tempxk.size()-1);
  TargetFinale={xtf, ytf, tetatf};
  flagOpt = 1;
  flagTF=1;
  //std::cout<<tempxk<<std::endl;
  flagMemorizzaTraiettoria=1;
  
}

void Controller::clockCallbackTEST(const rosgraph_msgs::Clock rclock)
{
  robot_clock=rclock.clock;
}


int main(int argc, char *argv[])
{
  printf(".. creo rosnode mbnodecontrollerifopt \n");

  FtempiOttimizzazione.open ("OUT_ifopt_TempiOttimizzazione.txt");
  FtempiCPU.open ("OUT_ifopt_TempiCPU.txt");
  FdurataPercorso.open ("OUT_ifopt_DurataPercorso.txt");
  FdistanzaPercorsa.open ("OUT_ifopt_DistanzaPercorsa.txt");
  
  ros::init(argc, argv, "mbnodecontrollerifopt");
  ros::NodeHandle node;
  if (!ros::master::check())
    return (0);

  Controller controller(node);

  // ros::Subscriber subClock = node.subscribe("/clock", 1, &Controller::clockCallbackTEST, &controller);
  // LABORATORIO
  // ros::Subscriber subPioneer_pose = node.subscribe("/vrpn_client_node/epuck_robot_4/pose", 1, &Controller::ePuck_poseCallbackTEST, &controller);
  ros::Subscriber subPioneer_pose = node.subscribe("/ePuck2/pose", 1, &Controller::ePuck_poseCallbackTEST, &controller);
  ros::Subscriber subTarget = node.subscribe("/target", 1, &Controller::targetCallback, &controller);
  ros::Subscriber subTrajectory = node.subscribe("/trajectory", 1, &Controller::trajectoryCallback, &controller);
  
  ros::Timer timer1 = node.createTimer(ros::Duration(0.1), &Controller::timer1CallbackTEST, &controller);

	// 10 Hz
	ros::Rate loop_rate(10); 
  ros::spin();

  FtempiOttimizzazione.close();
  FtempiCPU.close();
  FdurataPercorso.close();
  FdistanzaPercorsa.close();

  return 0;
}
