/******************************************************************
 *   © 2020 Matteo Bigliardi
 *   email: 239958@studenti.unimore.it
 * 
 *   mbnodecontrollerifopt.cpp
 * 
 ******************************************************************/


#include <ifopt/problem.h>
#include <ifopt/ipopt_solver.h>
#include <ifopt/variable_set.h>
#include <ifopt/constraint_set.h>
#include <ifopt/cost_term.h>
#include <Eigen/Dense>
#include <iostream>
#include <fstream>
#include <chrono>

#include <stdint.h> 
#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>      
#include <time.h> 


#include <ros/ros.h>
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"

#include "rosgraph_msgs/Clock.h"
#include "geometry_msgs/TransformStamped.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Pose2D.h"
#include "nav_msgs/Path.h"

//conta numero posizioni date
//int count= 0;

//scelta scena
int scena=1;


// variabili per memorizzare traiettoria in memoria
int flagMemorizzaTraiettoria =0;
const int maxElem = 5000;
std::vector<double> MemX(maxElem);
std::vector<double> MemY(maxElem);
int Elem = 0;
std::vector<double> TargetFinale (3);
//file per traiettoria
std::ofstream fTraiettoria;

//file tempi di ottimizzazione
std::ofstream FtempiOttimizzazione;

//file tempi di CPU
std::ofstream FtempiCPU;

//file durata percorso
int ho_iniziato = 0;
std::ofstream FdurataPercorso;
auto inizioPercorso = std::chrono::steady_clock::now();
auto finePercorso = std::chrono::steady_clock::now();

//file distanza percorsa
std::ofstream FdistanzaPercorsa;
double G_distanza_percorsa = 0;



//ostacoli
double ro= 0.12;
std::vector <double> xo (4);
std::vector <double> yo (4);

//flag  se in trajectory following flagTF=1
int flagTF =0;

//primo_target
bool primo_target=true;

//flag inizio ottimizzazione
int flagOpt=0;

//----DatiPosizione--------------------------------------
Eigen::VectorXd DPxk(2700);
int ScorriDPxk=0;
Eigen::VectorXd tempxk(2700);

//-----VarGlobali---------------------------------------------
double pi = 3.14159265358;
std::vector<double> G_posizione_iniziale (3);

std::vector<double> G_vel_precedente {0,0};

double G_C1, G_C2, G_C3, G_C4, G_C5, G_C6, G_C7, G_C8 =0;
std::vector<double> G_C_h {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};



//implementazione funzioni ifopt
namespace ifopt
{
  using Eigen::VectorXd;

  class ExVariables : public VariableSet
  {
  public:
    // Every variable set has a name, here "var_set1". this allows the constraints
    // and costs to define values and Jacobians specifically w.r.t this variable set.
    ExVariables() : ExVariables("var_set1"){};
    ExVariables(const std::string &name) : VariableSet(6, name)
    {
      // the initial values where the NLP starts iterating from
     
      du11 = 0.2;
      du12 =-0.2;
      du21 = 0;
      du22 = 0;
      du31 = 0;
      du32 = 0;
    }

    // Here is where you can transform the Eigen::Vector into whatever
    // internal representation of your variables you have 
    // can also be complex classes such as splines, etc..

    //  void SetVariables(const VectorXd& x) override
    void SetVariables(const VectorXd &du) override
    {
      
      du11 = du(0);
      du12 = du(1);
      du21 = du(2);
      du22 = du(3);
      du31 = du(4);
      du32 = du(5);
    };

    // Here is the reverse transformation from the internal representation to
    // to the Eigen::Vector
    VectorXd GetValues() const override
    {
      Eigen::VectorXd wXd(6);
      wXd << du11, du12,
             du21, du22,
             du31, du32;
      return wXd;
    };

    // Each variable has an upper and lower bound set here
    VecBound GetBounds() const override
    {
      VecBound bounds(GetRows());
      bounds.at(0) = Bounds(-0.2, 0.2);
      bounds.at(1) = Bounds(-0.2, 0.2);
      bounds.at(2) = Bounds(-0.2, 0.2);
      bounds.at(3) = Bounds(-0.2, 0.2);
      bounds.at(4) = Bounds(-0.2, 0.2);
      bounds.at(5) = Bounds(-0.2, 0.2);
      return bounds;
    }

  private:
   
    double du11, du12,
           du21, du22,
           du31, du32;
  };


//posizione funzione di costo

   Eigen::VectorXd mobRobCT0(Eigen::VectorXd x, Eigen::VectorXd u)
    {
      //Continuous-time nonlinear model of a differential drive mobile robot

      // 3 states (x):
      //   robot x position (x)
      //   robot y position (y)
      //  robot yaw angle (theta)
      //2 inputs (u):
      //linear velocity (v_r)
      //angular velocity(w_r)
      double theta_r = x(2);
      Eigen::VectorXd dxdt = x;
      double v_r = u(0);
      double w_r = u(1);
      dxdt(0) = cos(theta_r) * v_r;
      dxdt(1) = sin(theta_r) * v_r;
      dxdt(2) = w_r;
      Eigen::VectorXd y = x;

      return dxdt;
    };
    //-------wrapToPi-----------------------------------------------------
    double wrapToPi(double x)
    {
      x = fmod(x + pi,2*pi);
      if (x < 0)
          x += 2*pi;
      return x - pi;
    };

    // -----mobRobDT0-----------------------------------------------------
    Eigen::VectorXd mobRobDT0(Eigen::VectorXd xk, Eigen::VectorXd uk, double Ts)
    {
      int M = 10;
      double delta = Ts / M;
      Eigen::VectorXd xk1 = xk;

      for (int i = 0; i < M; i++)
      {
        xk1 = xk1 + delta*mobRobCT0(xk1,uk); 
        
      }

      //wraptopi
      xk1(2)= wrapToPi(xk1(2));
      Eigen::VectorXd yk = xk;

      return xk1;
    };


    // -------mobRobCostFCN-----------------------------------------------
    double mobRobCostFCN(Eigen::VectorXd du, Eigen::VectorXd x0, double Ts, int N, int M, Eigen::MatrixXd xref, Eigen::VectorXd u1)
    {
      /* Cost function of nonlinear MPC for mobile robot control

      Inputs:
      du:      optimization variable, from time k to time k+M-1 as a column vector
      x:      current state at time k (now)
      Ts:     controller sample time
      N:      prediction horizon
      M:      control horizon
      xref:   state references as a matrix whose N+1(from instant 0 to N) rows are the reference for the system

      u1:     previous controller output at time k-1
      J:      objective function cost

      NonlinearTs MPC design parameters
      */


      //Q matrix penalizes state deviations from the references.
      Eigen::DiagonalMatrix<double, 3> Q(12, 19, 0.1);
      
      //terminal cost matrix
      Eigen::DiagonalMatrix<double, 3> P = 100 * Q;

      // Rdu matrix penalizes the input rate of change (du).
      Eigen::DiagonalMatrix<double, 2> Rdu(8, 6);

      // Ru matrix penalizes the input magnitude
      Eigen::DiagonalMatrix<double, 2> Ru(8, 6);

      //init ref_k
      Eigen::VectorXd ref_k(3);
      ref_k << 0, 0, 0;

      //init theta_d
      double theta_d = 0.0;

      //init ek
      Eigen::VectorXd ek(3);
      ek << 0, 0, 0;

      // reshape du to a matrix which rows are du[k] from k to k+N, filling
      // rows after M with zeros
      Eigen::MatrixXd alldu(N, 2);
      alldu.setZero(N, 2);
      int w = 0;
      for (int i = 0; i < M; ++i)
      {
        for (int c = 0; c < 2; ++c)
        {
          alldu(i, c) = du(w);
          w++;
        }
      }

      /*
     Cost Calculation
     Set initial plant states and cost.
     */

      //xk = x0;
      Eigen::VectorXd xk(3);
      xk << 0, 0, 0;
      for (int i = 0; i < 3; i++)
      {
        xk(i) = x0(i);
      }

      //uk = u1;
      Eigen::VectorXd uk(2);
      uk << 0, 0;
      for (int i = 0; i < 2; i++)
      {
        uk(i) = u1(i);
      }

      //init J
      double j = 0;

      for (int i = 0; i < N; i++)
      {
        Eigen::VectorXd du_k(2);
        du_k << 0, 0;
        for (int j = 0; j < 2; j++)
        {
          du_k(j) = alldu(i, j);
        }
        du_k = du_k.transpose();
        uk = uk + du_k;

        for (int j = 0; j < 3; j++)
        {
          ref_k(j) = xref(i, j);
        }
        ref_k = ref_k.transpose();
        theta_d = ref_k(2);

        //Rot
        Eigen::MatrixXd Rot(3, 3);
        Rot.setZero(3, 3);
        
        Rot << cos(theta_d), sin(theta_d), 0, (-sin(theta_d)), cos(theta_d), 0, 0, 0, 1;

        

        ek = Rot * (xk - ref_k);

        /*std::cout<<"errooooooooooooooore"<<std::endl;
        std::cout<<"xk="<<xk<<std::endl;
        std::cout<<"ref_k="<<ref_k<<std::endl;
        std::cout<<"errooooooooooooooore"<<std::endl;*/

        j = j + ek.transpose() * Q * ek;
        j = j + du_k.transpose() * Rdu * du_k;
        j = j + uk.transpose() * Ru * uk;

        //obtain plant state at next prediction step
        xk = mobRobDT0(xk, uk, Ts);
      }

      //terminal cost
      for (int j = 0; j < 3; j++)
      {
        ref_k(j) = xref(N, j);
      }

      ref_k = ref_k.transpose();

      theta_d = ref_k(2);

      Eigen::MatrixXd Rot(3, 3);
      Rot.setZero(3, 3);

      Rot << cos(theta_d), sin(theta_d), 0, (-sin(theta_d)), cos(theta_d), 0, 0, 0, 1;
      ek = Rot * (xk - ref_k);

      j = j + ek.transpose() * P * ek;

      //output J
      //std::cout << "J = " <<std::to_string(j) << std::endl;

      return j;
    };

    double FunzioneDiCosto(Eigen::VectorXd du)
    {

      int M = 3;
      int N = 15;

      Eigen::VectorXd x0(3);
      x0<<G_posizione_iniziale[0],G_posizione_iniziale[1],G_posizione_iniziale[2];
    
      double Ts = 0.1;

      Eigen::MatrixXd xref(N + 1, 3);
      xref.setZero(N + 1, 3);

      //aggiungo ref !=0
      for (int i = 0; i < N+1; ++i)
      {
        
        for (int c = 0; c < 3; ++c)
        {
          if(ScorriDPxk+c+3*i<=(DPxk.size()-4)){
            xref(i,c)=DPxk(ScorriDPxk+c+3*i);
          } else {
            xref(i,c)=DPxk((DPxk.size()-3)+c);

          }
         
        }
      }
      
      Eigen::VectorXd u1(2);
      u1<<G_vel_precedente[0],G_vel_precedente[1];

      //ottengo J
      double j = mobRobCostFCN(du, x0, Ts, N, M, xref, u1);
      return j;
    };

    // fine funzione di costo


  class ExConstraint : public ConstraintSet
  {
  public:
    ExConstraint() : ExConstraint("constraint1") {}

    // This constraint set just contains 1 constraint, however generally
    // each set can contain multiple related constraints.
    ExConstraint(const std::string &name) : ConstraintSet(6, name) {}

    // The constraint value minus the constant value "1", moved to bounds.
    VectorXd GetValues() const override
    {

      VectorXd g(GetRows());
      VectorXd du = GetVariables()->GetComponent("var_set1")->GetValues();
      //matrice per calcolo wr wl
      Eigen::MatrixXd M(6, 6);
      M.setZero(6, 6);
      M(0,0)=1/r;
      M(0,1)=d/r;
      M(1,0)=1/r;
      M(1,1)=-d/r;
      M(2,2)=1/r;
      M(2,3)=d/r;
      M(3,2)=1/r;
      M(3,3)=-d/r;
      M(4,4)=1/r;
      M(4,5)=d/r;
      M(5,4)=1/r;
      M(5,5)=-d/r;

      //calcolo velocità istanti successivi
      double v1= G_vel_precedente[0]+du(0);
      double w1= G_vel_precedente[1]+du(1);
      double v2= G_vel_precedente[0]+du(0)+du(2);
      double w2= G_vel_precedente[1]+du(1)+du(3);
      double v3= G_vel_precedente[0]+du(0)+du(2)+du(4);
      double w3= G_vel_precedente[1]+du(1)+du(3)+du(5);

      Eigen::VectorXd u(6);
      u<<v1,w1,v2,w2,v3,w3;

      //calcolo wr wl istanti successivi
      Eigen::VectorXd temp(6);
      temp=M*u;

      //inserisco wr e wl nel vettore dei vincoli
      for (int i = 0; i < 6; i++)
      {
        g(i)=temp(i);
      }
      
      //inizio calcolo per la previsione delle posizioni successive
      Eigen::VectorXd xin(3);
      xin<<G_posizione_iniziale[0],G_posizione_iniziale[1],G_posizione_iniziale[2];
      double T=0.1;
      
      Eigen::VectorXd uk1(2);
      uk1<<v1, w1;
      Eigen::VectorXd uk2(2);
      uk2<<v2, w2;
      Eigen::VectorXd uk3(2);
      uk3<<v3, w3;

      Eigen::VectorXd xk1(3);
      xk1 = mobRobDT0(xin, uk1, T);
      Eigen::VectorXd xk2(3);
      xk2 = mobRobDT0(xk1, uk2, T);
      Eigen::VectorXd xk3(3);
      xk3 = mobRobDT0(xk2, uk3, T);
      Eigen::VectorXd xk4(3);
      xk4 = mobRobDT0(xk3, uk3, T);
      Eigen::VectorXd xk5(3);
      xk5 = mobRobDT0(xk4, uk3, T);
      Eigen::VectorXd xk6(3);
      xk6 = mobRobDT0(xk5, uk3, T);
      Eigen::VectorXd xk7(3);
      xk7 = mobRobDT0(xk6, uk3, T);
      Eigen::VectorXd xk8(3);
      xk8 = mobRobDT0(xk7, uk3, T);
      Eigen::VectorXd xk9(3);
      xk9 = mobRobDT0(xk8, uk3, T);
      Eigen::VectorXd xk10(3);
      xk10 = mobRobDT0(xk9, uk3, T);
      Eigen::VectorXd xk11(3);
      xk11 = mobRobDT0(xk10, uk3, T);
      Eigen::VectorXd xk12(3);
      xk12 = mobRobDT0(xk11, uk3, T);
     /* Eigen::VectorXd xk13(3);
      xk13 = mobRobDT0(xk12, uk3, T);
      Eigen::VectorXd xk14(3);
      xk14 = mobRobDT0(xk13, uk3, T);
      Eigen::VectorXd xk15(3);
      xk15 = mobRobDT0(xk14, uk3, T);*/
    
      double x12= xk12(0);
      double y12= xk12(1);
      //double x15= xk15(0);
      //double y15= xk15(1);

      //if(scena==1){
        //xo= {0.40, 0.85, 1.10, 1.20};
        //yo= {0.30, 0.65, 0.82, 0.55};
      //}else if(scena==2){
        xo= {0.50, 1.00, 1.00, 1.00};
        yo= {0.50, 0.50, 0.25, 0.75};
      //}
        // xo= {50, 100, 100, 100};
        // yo= {50, 50, 25, 75};
     
      
      //distanza dagli ostacoli
      double C1= sqrt((y12-yo[0])*(y12-yo[0])+(x12-xo[0])*(x12-xo[0]));
      double C2= sqrt((y12-yo[1])*(y12-yo[1])+(x12-xo[1])*(x12-xo[1]));
      double C3= sqrt((y12-yo[2])*(y12-yo[2])+(x12-xo[2])*(x12-xo[2]));
      double C4= sqrt((y12-yo[3])*(y12-yo[3])+(x12-xo[3])*(x12-xo[3]));

      /*double C5= sqrt((y15-yo[0])*(y15-yo[0])+(x15-xo[0])*(x15-xo[0]));
      double C6= sqrt((y15-yo[1])*(y15-yo[1])+(x15-xo[1])*(x15-xo[1]));
      double C7= sqrt((y15-yo[2])*(y15-yo[2])+(x15-xo[2])*(x15-xo[2]));
      double C8= sqrt((y15-yo[3])*(y15-yo[3])+(x15-xo[3])*(x15-xo[3]));*/

      /*double C2= sqrt((y6-yo[1])*(y6-yo[1])+(x6-xo[1])*(x6-xo[1]));
      double C3= sqrt((y6-yo[2])*(y6-yo[2])+(x6-xo[2])*(x6-xo[2]));
      double C4= sqrt((y6-yo[3])*(y6-yo[3])+(x6-xo[3])*(x6-xo[3]));
      double C5= sqrt((y6-yo[4])*(y6-yo[4])+(x6-xo[4])*(x6-xo[4]));
      double C6= sqrt((y6-yo[5])*(y6-yo[5])+(x6-xo[5])*(x6-xo[5]));
      double C7= sqrt((y6-yo[6])*(y6-yo[6])+(x6-xo[6])*(x6-xo[6]));*/
      

      G_C1= C1;
      G_C2= C2;
      G_C3= C3;
      G_C4= C4;
      /*G_C5= C5;
      G_C6= C6;
      G_C7= C7;
      G_C8= C8;*/
      

      G_C_h.clear();
      
      //inizio calcolo per trovare derivata approssimata
      for (int j = 0; j < 6; j++)
      {
        du(j)=du(j)+0.001;

        

        double v1_h= G_vel_precedente[0]+du(0);
        double w1_h= G_vel_precedente[1]+du(1);
        double v2_h= G_vel_precedente[0]+du(0)+du(2);
        double w2_h= G_vel_precedente[1]+du(1)+du(3);
        double v3_h= G_vel_precedente[0]+du(0)+du(2)+du(4);
        double w3_h= G_vel_precedente[1]+du(1)+du(3)+du(5);

        Eigen::VectorXd uk1_h(2);
        uk1_h<<v1_h, w1_h;
        Eigen::VectorXd uk2_h(2);
        uk2_h<<v2_h, w2_h;
        Eigen::VectorXd uk3_h(2);
        uk3_h<<v3_h, w3_h;

        Eigen::VectorXd xk1_h(3);
        xk1_h = mobRobDT0(xin, uk1_h, T);
        Eigen::VectorXd xk2_h(3);
        xk2_h = mobRobDT0(xk1_h, uk2_h, T);
        Eigen::VectorXd xk3_h(3);
        xk3_h = mobRobDT0(xk2_h, uk3_h, T);
        Eigen::VectorXd xk4_h(3);
        xk4_h = mobRobDT0(xk3_h, uk3_h, T);
        Eigen::VectorXd xk5_h(3);
        xk5_h = mobRobDT0(xk4_h, uk3_h, T);
        Eigen::VectorXd xk6_h(3);
        xk6_h = mobRobDT0(xk5_h, uk3_h, T);
        Eigen::VectorXd xk7_h(3);
        xk7_h = mobRobDT0(xk6_h, uk3_h, T);
        Eigen::VectorXd xk8_h(3);
        xk8_h = mobRobDT0(xk7_h, uk3_h, T);
        Eigen::VectorXd xk9_h(3);
        xk9_h = mobRobDT0(xk8_h, uk3_h, T);
        Eigen::VectorXd xk10_h(3);
        xk10_h = mobRobDT0(xk9_h, uk3_h, T);
        Eigen::VectorXd xk11_h(3);
        xk11_h = mobRobDT0(xk10_h, uk3_h, T);
        Eigen::VectorXd xk12_h(3);
        xk12_h = mobRobDT0(xk11_h, uk3_h, T);
        /*Eigen::VectorXd xk13_h(3);
        xk13_h = mobRobDT0(xk12_h, uk3_h, T);
        Eigen::VectorXd xk14_h(3);
        xk14_h = mobRobDT0(xk13_h, uk3_h, T);
        Eigen::VectorXd xk15_h(3);
        xk15_h = mobRobDT0(xk14_h, uk3_h, T);*/
    
        //double x15_h= xk15_h(0);
        //double y15_h= xk15_h(1);
        double x12_h= xk12_h(0);
        double y12_h= xk12_h(1);

        double C1_h= sqrt((y12_h-yo[0])*(y12_h-yo[0])+(x12_h-xo[0])*(x12_h-xo[0]));
        double C2_h= sqrt((y12_h-yo[1])*(y12_h-yo[1])+(x12_h-xo[1])*(x12_h-xo[1]));
        double C3_h= sqrt((y12_h-yo[2])*(y12_h-yo[2])+(x12_h-xo[2])*(x12_h-xo[2]));
        double C4_h= sqrt((y12_h-yo[3])*(y12_h-yo[3])+(x12_h-xo[3])*(x12_h-xo[3]));

        /*double C5_h= sqrt((y15_h-yo[0])*(y15_h-yo[0])+(x15_h-xo[0])*(x15_h-xo[0]));
        double C6_h= sqrt((y15_h-yo[1])*(y15_h-yo[1])+(x15_h-xo[1])*(x15_h-xo[1]));
        double C7_h= sqrt((y15_h-yo[2])*(y15_h-yo[2])+(x15_h-xo[2])*(x15_h-xo[2]));
        double C8_h= sqrt((y15_h-yo[3])*(y15_h-yo[3])+(x15_h-xo[3])*(x15_h-xo[3]));*/
        
       /* double C5_h= sqrt((y6_h-yo[4])*(y6_h-yo[4])+(x6_h-xo[4])*(x6_h-xo[4]));
        double C6_h= sqrt((y6_h-yo[5])*(y6_h-yo[5])+(x6_h-xo[5])*(x6_h-xo[5]));
        double C7_h= sqrt((y6_h-yo[6])*(y6_h-yo[6])+(x6_h-xo[6])*(x6_h-xo[6]));*/
        

        du(j)=du(j)-0.001;

       
        G_C_h.push_back(C1_h);
        G_C_h.push_back(C2_h);
        G_C_h.push_back(C3_h);
        G_C_h.push_back(C4_h);
        /*G_C_h.push_back(C5_h);
        G_C_h.push_back(C6_h);
        G_C_h.push_back(C7_h);
        G_C_h.push_back(C8_h);*/
      }
      
      Eigen::VectorXd temp1(4);
      temp1<<C1,C2,C3,C4; 
      //,C5,C6,C7,C8;
      
      //inserisco distanza dagli ostacoli nel vettore dei vincoli 
      /*for (int i = 6; i < 10; i++)
      {
        g(i)=temp1(i-6);
      }*/
      
      
      
      return g;
    };

    
    // Constant values should always be put into GetBounds(), not GetValues().
    // For inequality constraints (<,>), use Bounds(x, inf) or Bounds(-inf, x).
    VecBound GetBounds() const override
    {
      
      VecBound b(GetRows());

      // velocity bounds
      b.at(0) = Bounds(-1.5*pi,1.5* pi);
      b.at(1) = Bounds(-1.5*pi,1.5* pi);
      b.at(2) = Bounds(-1.5*pi,1.5* pi);
      b.at(3) = Bounds(-1.5*pi,1.5* pi);
      b.at(4) = Bounds(-1.5*pi,1.5* pi);
      b.at(5) = Bounds(-1.5*pi, 1.5*pi);

      // position bounds
      /*b.at(6) = Bounds(ro, inf);
      b.at(7) = Bounds(ro, inf);
      b.at(8) = Bounds(ro, inf);
      b.at(9) = Bounds(ro, inf);*/
     /* b.at(10) = Bounds(ro, inf);
      b.at(11) = Bounds(ro, inf);
      b.at(12) = Bounds(ro, inf);
      b.at(13) = Bounds(ro, inf);*/
      
     return b;
    }

    // This function provides the first derivative of the constraints.
    // In case this is too difficult to write, you can also tell the solvers to
    // approximate the derivatives by finite differences and not overwrite this
    // function, e.g. in ipopt.cc::use_jacobian_approximation_ = true
    void FillJacobianBlock(std::string var_set, Jacobian &jac_block) const override
    {

      // must fill only that submatrix of the overall Jacobian that relates
      // to this constraint and "var_set1". even if more constraints or variables
      // classes are added, this submatrix will always start at row 0 and column 0,
      // thereby being independent from the overall problem.

      

      if (var_set == "var_set1")
      {
        VectorXd du = GetVariables()->GetComponent("var_set1")->GetValues();

        //velocita' 
        jac_block.coeffRef(0, 0) = 1/r;
        jac_block.coeffRef(0, 1) = d/r;
        jac_block.coeffRef(0, 2) = 0.0;
        jac_block.coeffRef(0, 3) = 0.0;
        jac_block.coeffRef(0, 4) = 0.0;
        jac_block.coeffRef(0, 5) = 0.0;
        
        jac_block.coeffRef(1, 0) = 1/r;
        jac_block.coeffRef(1, 1) = -d/r;
        jac_block.coeffRef(1, 2) = 0.0;
        jac_block.coeffRef(1, 3) = 0.0;
        jac_block.coeffRef(1, 4) = 0.0;
        jac_block.coeffRef(1, 5) = 0.0;

        jac_block.coeffRef(2, 0) = 1/r;
        jac_block.coeffRef(2, 1) = d/r;
        jac_block.coeffRef(2, 2) = 1/r;
        jac_block.coeffRef(2, 3) = d/r;
        jac_block.coeffRef(2, 4) = 0.0;
        jac_block.coeffRef(2, 5) = 0.0;

        jac_block.coeffRef(3, 0) = 1/r;
        jac_block.coeffRef(3, 1) = -d/r;
        jac_block.coeffRef(3, 2) = 1/r;
        jac_block.coeffRef(3, 3) = -d/r;
        jac_block.coeffRef(3, 4) = 0.0;
        jac_block.coeffRef(3, 5) = 0.0;

        jac_block.coeffRef(4, 0) = 1/r;
        jac_block.coeffRef(4, 1) = d/r;
        jac_block.coeffRef(4, 2) = 1/r;
        jac_block.coeffRef(4, 3) = d/r;
        jac_block.coeffRef(4, 4) = 1/r;
        jac_block.coeffRef(4, 5) = d/r;

        jac_block.coeffRef(5, 0) = 1/r;
        jac_block.coeffRef(5, 1) = -d/r;
        jac_block.coeffRef(5, 2) = 1/r;
        jac_block.coeffRef(5, 3) = -d/r;
        jac_block.coeffRef(5, 4) = 1/r;
        jac_block.coeffRef(5, 5) = -d/r;



        //posizione
        double h=0.001;

        /*jac_block.coeffRef(6, 0) = (G_C_h[0]-G_C1)/h;
        jac_block.coeffRef(6, 1) = (G_C_h[4]-G_C1)/h;
        jac_block.coeffRef(6, 2) = (G_C_h[8]-G_C1)/h;
        jac_block.coeffRef(6, 3) = (G_C_h[12]-G_C1)/h;
        jac_block.coeffRef(6, 4) = (G_C_h[16]-G_C1)/h;
        jac_block.coeffRef(6, 5) = (G_C_h[20]-G_C1)/h;

        jac_block.coeffRef(7, 0) = (G_C_h[1]-G_C2)/h;
        jac_block.coeffRef(7, 1) = (G_C_h[5]-G_C2)/h;
        jac_block.coeffRef(7, 2) = (G_C_h[9]-G_C2)/h;
        jac_block.coeffRef(7, 3) = (G_C_h[13]-G_C2)/h;
        jac_block.coeffRef(7, 4) = (G_C_h[17]-G_C2)/h;
        jac_block.coeffRef(7, 5) = (G_C_h[21]-G_C2)/h;

        jac_block.coeffRef(8, 0) = (G_C_h[2]-G_C3)/h;
        jac_block.coeffRef(8, 1) = (G_C_h[6]-G_C3)/h;
        jac_block.coeffRef(8, 2) = (G_C_h[10]-G_C3)/h;
        jac_block.coeffRef(8, 3) = (G_C_h[14]-G_C3)/h;
        jac_block.coeffRef(8, 4) = (G_C_h[18]-G_C3)/h;
        jac_block.coeffRef(8, 5) = (G_C_h[22]-G_C3)/h;

        jac_block.coeffRef(9, 0) = (G_C_h[3]-G_C4)/h;
        jac_block.coeffRef(9, 1) = (G_C_h[7]-G_C4)/h;
        jac_block.coeffRef(9, 2) = (G_C_h[11]-G_C4)/h;
        jac_block.coeffRef(9, 3) = (G_C_h[15]-G_C4)/h;
        jac_block.coeffRef(9, 4) = (G_C_h[19]-G_C4)/h;
        jac_block.coeffRef(9, 5) = (G_C_h[23]-G_C4)/h;*/

        /*jac_block.coeffRef(10, 0) = (G_C_h[4]-G_C5)/h;
        jac_block.coeffRef(10, 1) = (G_C_h[12]-G_C5)/h;
        jac_block.coeffRef(10, 2) = (G_C_h[20]-G_C5)/h;
        jac_block.coeffRef(10, 3) = (G_C_h[28]-G_C5)/h;
        jac_block.coeffRef(10, 4) = (G_C_h[36]-G_C5)/h;
        jac_block.coeffRef(10, 5) = (G_C_h[44]-G_C5)/h;

        jac_block.coeffRef(11, 0) = (G_C_h[5]-G_C6)/h;
        jac_block.coeffRef(11, 1) = (G_C_h[13]-G_C6)/h;
        jac_block.coeffRef(11, 2) = (G_C_h[21]-G_C6)/h;
        jac_block.coeffRef(11, 3) = (G_C_h[29]-G_C6)/h;
        jac_block.coeffRef(11, 4) = (G_C_h[37]-G_C6)/h;
        jac_block.coeffRef(11, 5) = (G_C_h[45]-G_C6)/h;

        jac_block.coeffRef(12, 0) = (G_C_h[6]-G_C7)/h;
        jac_block.coeffRef(12, 1) = (G_C_h[14]-G_C7)/h;
        jac_block.coeffRef(12, 2) = (G_C_h[22]-G_C7)/h;
        jac_block.coeffRef(12, 3) = (G_C_h[30]-G_C7)/h;
        jac_block.coeffRef(12, 4) = (G_C_h[38]-G_C7)/h;
        jac_block.coeffRef(12, 5) = (G_C_h[46]-G_C7)/h;

        jac_block.coeffRef(13, 0) = (G_C_h[7]-G_C8)/h;
        jac_block.coeffRef(13, 1) = (G_C_h[15]-G_C8)/h;
        jac_block.coeffRef(13, 2) = (G_C_h[23]-G_C8)/h;
        jac_block.coeffRef(13, 3) = (G_C_h[31]-G_C8)/h;
        jac_block.coeffRef(13, 4) = (G_C_h[39]-G_C8)/h;
        jac_block.coeffRef(13, 5) = (G_C_h[47]-G_C8)/h;*/





      }

    }
    private:
      double r= 0.020;
      double d= 0.053/2;
     /* double ro= 0.12;
      
      
      std::vector <double> xo= {0.4, 0.85, 0.9, 1.2};
      std::vector <double> yo= {0.3, 0.65, 0.15, 0.4};*/
      
  };

  class ExCost : public CostTerm
  {
  public:
    ExCost() : ExCost("cost_term1") {}
    ExCost(const std::string &name) : CostTerm(name) {}

    double GetCost() const override
    {
      
      VectorXd du = GetVariables()->GetComponent("var_set1")->GetValues();
      double j = FunzioneDiCosto(du);
      return j;
    };

    void FillJacobianBlock(std::string var_set, Jacobian &jac) const override
    {
      if (var_set == "var_set1")
      {
        VectorXd du = GetVariables()->GetComponent("var_set1")->GetValues();

        
        Eigen::VectorXd h(6);
        h << 0.001, 0.001, 0.001, 0.001, 0.001, 0.001; 

      
     // std::cout << "--------------------------------------"<< std::endl;
         double FC = FunzioneDiCosto(du);
         du(0)= du(0) + h(0);
         double FC_h = FunzioneDiCosto(du);
         //std::cout << "J = " <<std::to_string(FC) << std::endl;
        // std::cout << "J_h = " <<std::to_string(FC_h) << std::endl;
        
         double derivata_approssimata_u11= (FC_h-FC)/0.001;

         du(0)= du(0) - h(0);
         du(1)= du(1) + h(1);
         FC_h = FunzioneDiCosto(du);
        // std::cout << "J_h = " <<std::to_string(FC_h) << std::endl;
        
         double derivata_approssimata_u12= (FC_h-FC)/0.001;

         du(1)= du(1) - h(1);
         du(2)= du(2) + h(2);
         FC_h = FunzioneDiCosto(du);
        // std::cout << "J_h = " <<std::to_string(FC_h) << std::endl;
        
         double derivata_approssimata_u21= (FC_h-FC)/0.001;

         du(2)= du(2) - h(2);
         du(3)= du(3) + h(3);
         FC_h = FunzioneDiCosto(du);
         //std::cout << "J_h = " <<std::to_string(FC_h) << std::endl;
         double derivata_approssimata_u22= (FC_h-FC)/0.001;

         du(3)= du(3) - h(3);
         du(4)= du(4) + h(4);
         FC_h = FunzioneDiCosto(du);
         //std::cout << "J_h = " <<std::to_string(FC_h) << std::endl;
         double derivata_approssimata_u31= (FC_h-FC)/0.001;

         du(4)= du(4) - h(4);
         du(5)= du(5) + h(5);
         FC_h = FunzioneDiCosto(du);
         du(5)= du(5) - h(5);
        // std::cout << "J_h = " <<std::to_string(FC_h) << std::endl;
         double derivata_approssimata_u32= (FC_h-FC)/0.001;
         //std::cout << "--------------------------------------"<< std::endl;
         //std::cout <<du<<std::endl;

        jac.coeffRef(0, 0) = derivata_approssimata_u11;
        jac.coeffRef(0, 1) = derivata_approssimata_u12;
        jac.coeffRef(0, 2) = derivata_approssimata_u21;
        jac.coeffRef(0, 3) = derivata_approssimata_u22;
        jac.coeffRef(0, 4) = derivata_approssimata_u31;
        jac.coeffRef(0, 5) = derivata_approssimata_u32;
  
      }
    };

  };

} // namespace ifopt






//class controller
class Controller
{
public:
  Controller(ros::NodeHandle &nh);

  //void ePuck_poseCallbackTEST(const geometry_msgs::PoseStamped trans);
  void ePuck_poseCallbackTEST(const geometry_msgs::TransformStamped trans);
  void clockCallbackTEST(const rosgraph_msgs::Clock rclock);
  void timer1CallbackTEST(const ros::TimerEvent &);
  void targetCallback(const geometry_msgs::Pose2D target );
  void trajectoryCallback(const nav_msgs::Path trajectory);
  
protected:
  ros::Publisher commandPub;
  ros::Time robot_clock;

  
};
 
Controller::Controller(ros::NodeHandle &nh)
{
  //commandPub = nh.advertise<geometry_msgs::Twist>("/epuck_robot_4/mobile_base/cmd_vel", 10);
  commandPub = nh.advertise<geometry_msgs::Twist>("/ePuck2/cmd_vel", 10);
  
  ROS_INFO("..creata istanza controller");
};

void Controller::timer1CallbackTEST(const ros::TimerEvent &)
{
  geometry_msgs::Twist msg;

  ifopt::Problem nlp;
  nlp.AddVariableSet  (std::make_shared<ifopt::ExVariables>());
  nlp.AddConstraintSet(std::make_shared<ifopt::ExConstraint>());
  nlp.AddCostSet      (std::make_shared<ifopt::ExCost>());
  //nlp.PrintCurrent();

  // 2. choose solver and options
  ifopt::IpoptSolver ipopt;
  ipopt.SetOption("linear_solver", "mumps");
  ipopt.SetOption("jacobian_approximation", "exact");
  ipopt.SetOption("max_iter", 3000);
  ipopt.SetOption("print_level", 0);
  ipopt.SetOption("acceptable_tol", 0.01);
  
  
  //incremento del contatore per il vector DPxk, inserito qui per seguire il looprate
  if(ScorriDPxk<((DPxk.size())-3)){
        ScorriDPxk=ScorriDPxk+3;
      }
  //std::cout<<"scorridpxk="<<std::to_string((ScorriDPxk))<<std::endl;
 //aggiungo raggiungimento della posizione iniziale prima del trajectory following
  double dist_pos_in = sqrt((DPxk(ScorriDPxk)-G_posizione_iniziale[0])*(DPxk(ScorriDPxk)-G_posizione_iniziale[0])+(DPxk(ScorriDPxk+1)-G_posizione_iniziale[1])*(DPxk(ScorriDPxk+1)-G_posizione_iniziale[1]));
  //std::cout<< std::to_string(dist_pos_in)<<std::endl;




  if(flagTF==1){
    
    if(dist_pos_in<=0.10){
      //std::cout<< "eccoooooooooooooooooooooooooooooooomi"<<std::endl;
      ScorriDPxk=0;
      //std::cout<< "fase 1"<<std::endl;
      DPxk=tempxk;
      //std::cout<< "fase 2"<<std::endl;
      //std::cout<<DPxk<<std::endl;
      //std::cout<< "fase 3"<<std::endl;
      flagTF=0;
    }
  }
  //std::cout<<"xref= "<<std::to_string(DPxk(ScorriDPxk))<<"yref= "<<std::to_string(DPxk(ScorriDPxk+1))<<"tetaref= "<<std::to_string(DPxk(ScorriDPxk+2))<<std::endl;

  //flag partenza ottimizzazione
  if (flagOpt == 1)
  {
    if (ho_iniziato == 0)   {
       ho_iniziato = 1;
       inizioPercorso = std::chrono::steady_clock::now();
    }
    

    //std::cout<<"flag=1"<<std::endl;

    //start misura tempo di ottimizzazione
    auto start = std::chrono::steady_clock::now();
    
    clock_t ct1, ct2;  
    if ((ct1 = clock ()) == -1)  
        perror ("clock");

    //inizio ottimizzazione
    ipopt.Solve(nlp);

    if ((ct2 = clock ()) == -1)      
        perror ("clock");

    FtempiCPU << ct2 - ct1 << "\n";

    //fine misura tempo di ottimizzazione
    auto end = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    FtempiOttimizzazione << elapsed_seconds.count() << "\n";
    
    //std::cout<<std::to_string(ipopt.GetReturnStatus())<<std::endl;
    //intercetto fallimento ottimizzatore
    if (ipopt.GetReturnStatus() == 2 || ipopt.GetReturnStatus() == -2 || ipopt.GetReturnStatus() == -13)
    {
      msg.linear.x = 0;
      msg.angular.z = 0;
      G_vel_precedente[0] = msg.linear.x;
      G_vel_precedente[1] = msg.angular.z;
    }
    else
    {
      Eigen::VectorXd opt_values = nlp.GetOptVariables()->GetValues();
      //std::cout<<opt_values(1)<<std::endl;
      //std::cout<<opt_values(0)<<std::endl;
      msg.linear.x = G_vel_precedente[0] + opt_values(0);
      msg.angular.z = G_vel_precedente[1] + opt_values(1);
      G_vel_precedente[0] = msg.linear.x;
      G_vel_precedente[1] = msg.angular.z;
      //std::cout<<"dpxk="<<"\n"<<DPxk<<std::endl;
    }
  }
  else if (flagOpt == 0)
  {
    //std::cout<<"flag=0"<<std::endl;

    msg.linear.x = 0;
    msg.angular.z = 0;
    G_vel_precedente[0] = msg.linear.x;
    G_vel_precedente[1] = msg.angular.z;
  }

  //msg.linear.x=(pi*2.5)/90;
  //msg.angular.z=-pi/90;
  
  // memorizzo traiettoria in memoria
  if (flagMemorizzaTraiettoria==1){
    MemX[Elem]=G_posizione_iniziale[0];
    MemY[Elem]=G_posizione_iniziale[1];
    //std::cout<<"..memorizzo posizione traiettoria!"<<std::endl;
    double deltaAng = abs(G_posizione_iniziale[2]-TargetFinale[2]);
    double delta = sqrt((G_posizione_iniziale[0]-TargetFinale[0])*(G_posizione_iniziale[0]-TargetFinale[0])+(G_posizione_iniziale[1]-TargetFinale[1])*(G_posizione_iniziale[1]-TargetFinale[1]));
    std::cout<<"delta = "<<std::to_string(delta)<<" deltaAng = "<<std::to_string(deltaAng)<<std::endl;
    if(delta <= 0.10 && deltaAng <= 0.05){
      
       finePercorso  = std::chrono::steady_clock::now();   
       std::chrono::duration<double> durata_percorso_in_secondi = finePercorso - inizioPercorso;
       FdurataPercorso << durata_percorso_in_secondi.count() << "\n";
      
       FdistanzaPercorsa << std::to_string(G_distanza_percorsa) << "\n";


       fTraiettoria.open ("OUT_ifopt_traiettoria.csv");
       flagMemorizzaTraiettoria=0;
       int k = 0;
       for (k=0; k<=Elem; k++){
          fTraiettoria << MemX[k] << ";" << MemY[k] << "\n";
          std::cout<<">>>>>>>>>>>>> SCRIVO RIGA TRAIETTORIA!!!!"<<std::endl;
       }
       fTraiettoria.close();
    }
    Elem = Elem+1;
  }
  
  commandPub.publish(msg);

  //double velLeft = 2*(G_vel_precedente[0] - (0.053 / 2.0) * G_vel_precedente[1]) / 0.040;
  //double velRight = 2*(G_vel_precedente[0] + (0.053 / 2.0) * G_vel_precedente[1]) / 0.040;
  
  //ROS_INFO("--------------------------------------------------------------------");
  //std::cout<<"x="<<std::to_string(G_posizione_iniziale[0])<<" "<<"y="<<std::to_string(G_posizione_iniziale[1])<<" "<<"teta="<<std::to_string(G_posizione_iniziale[2])<<std::endl;
  //ROS_INFO("--------------------------------------------------------------------");
  //ROS_INFO("--> /cmd_vel : left e right %f %f ", velLeft, velRight);
  //std::cout<<opt_values<<std::endl;
  //ROS_INFO("--------------------------------------------------------------------");

  
  
}

void Controller::ePuck_poseCallbackTEST(const geometry_msgs::TransformStamped trans)
{
  //std::cout<<"dentro a pose_callback"<<std::endl;

  //double x = trans.pose.position.x;
  //double y = trans.pose.position.y;
  //double z = trans.pose.orientation.z;
  double x = trans.transform.translation.x;
  double y = trans.transform.translation.y;
  double z = trans.transform.rotation.z;
  double w = trans.transform.rotation.w;
  //std::cout<<"w="<<std::to_string(w)<<std::endl;
  double teta=0;
  
  teta = (2*asin(z));
  if(teta>-pi/2 && teta< pi/2){
    teta= -teta;
  }else if (teta > pi/2 && teta <pi && w>0){
    teta=teta;
  }else if (teta >= pi/2 && teta <pi && w<0){
    teta= -teta;
  }
  //std::cout<<"x="<<std::to_string(x)<<" "<<"y="<<std::to_string(y)<<" "<<"teta="<<std::to_string(teta)<<std::endl;
  
   if(ho_iniziato==1 && flagMemorizzaTraiettoria==1 ){
     double tmp = sqrt((G_posizione_iniziale[0]-x)*(G_posizione_iniziale[0]-x)+(G_posizione_iniziale[1]-y)*(G_posizione_iniziale[1]-y));
     G_distanza_percorsa= G_distanza_percorsa+tmp;
   }

  
  double rx = ((rand() % 6)-3)/100;
  double ry = ((rand() % 6)-3)/100;
  double rteta = ((rand() % 6)-3)/100;
  x= x + rx;
  y= y + ry;
  teta= teta + rteta;

  
  
  
  //passaggio posizione a vector globale
  std::vector<double> v_pose ={x,y,teta};
  G_posizione_iniziale = v_pose;


  //count= count +1;
  //partenza ottimizzatore->si
  /*if(count>10){
    flagOpt = 1;
  }

  //impostazione primo target=posizione iniziale
  if(primo_target){
    DPxk.resize(2700);

    for (int k =0 ; k <= 2697; k=k+3)
    {
    
      DPxk(k) = x;
      DPxk(k+1) = y;
      DPxk(k+2) = teta;
    }
    primo_target=false;
    //std::cout<<"x="<<std::to_string(G_posizione_iniziale[0])<<" "<<"y="<<std::to_string(G_posizione_iniziale[1])<<" "<<"teta="<<std::to_string(G_posizione_iniziale[2])<<std::endl;
    //std::cout<<DPxk<<std::endl;
  }*/
  
  
  //std::cout<<"x="<<std::to_string(G_posizione_iniziale[0])<<" "<<"y="<<std::to_string(G_posizione_iniziale[1])<<" "<<"teta="<<std::to_string(G_posizione_iniziale[2])<<std::endl;
  
  
}


//intercettazione posizione desiderata
void Controller::targetCallback(const geometry_msgs::Pose2D target){
 

  ScorriDPxk=0;

  double x = target.x;
  double y = target.y;
  double teta = target.theta;
  
  teta = fmod(teta + pi,2*pi);
  if (teta < 0){
    teta += 2*pi;
  }
  teta= teta-pi;

  DPxk.resize(2700);

  for (int k =0 ; k <= 2697; k=k+3)
  {
    
    DPxk(k) = x;
    DPxk(k+1) = y;
    DPxk(k+2) = teta;
  }
  flagOpt = 1;
  TargetFinale={x, y, teta};
  flagMemorizzaTraiettoria=1;
 
  
}

//intercettazione traiettoria desiderata
void Controller::trajectoryCallback(const nav_msgs::Path trajectory){

  ScorriDPxk=0;

  double n_punti_traiettoria=trajectory.poses.size();
  
  DPxk.resize(n_punti_traiettoria*3);
  tempxk.resize(n_punti_traiettoria*3);

  double i=0;
  for(auto pose : trajectory.poses){
     double x = pose.pose.position.x;
     double y = pose.pose.position.y;
     double qz = pose.pose.orientation.z;
     double teta = -(2*asin(qz));
     tempxk(i)=x;
     tempxk(i+1)=y;
     tempxk(i+2)=teta;
     DPxk(i)=tempxk(0);
     DPxk(i+1)=tempxk(1);
     DPxk(i+2)=tempxk(2);
     i= i+3;
  }
  double xtf= tempxk(tempxk.size()-3);
  double ytf= tempxk(tempxk.size()-2);
  double tetatf= tempxk(tempxk.size()-1);
  TargetFinale={xtf, ytf, tetatf};
  flagOpt = 1;
  flagTF=1;
  //std::cout<<tempxk<<std::endl;
  flagMemorizzaTraiettoria=1;
  
}

void Controller::clockCallbackTEST(const rosgraph_msgs::Clock rclock)
{
  robot_clock=rclock.clock;

}


int main(int argc, char *argv[])
{
  printf(".. creo rosnode mbnodecontrollerifopt \n");
  
  
  // valorizzazione dati posizione con circa 300 punti
  // si presuppone circorferenza percorsa a velocità costante
  //DPxk(0) = -5;
  //DPxk(1) =  0;
  //DPxk(2) = pi/2;
 
  /*Eigen::VectorXd DPuk(2);
  DPuk<<(pi*2.5)/90, -pi/90;

  for (int k = 3; k <= 2697; k=k+3)
  {
    Eigen::VectorXd DP(3);
    DP<<DPxk(k-3),DPxk(k-2),DPxk(k-1);
    Eigen::VectorXd DPxk1;
    DPxk1= ifopt::mobRobDT0(DP, DPuk, 0.1);
    DPxk(k) = DPxk1(0);
    DPxk(k+1) = DPxk1(1);
    DPxk(k+2) = DPxk1(2);
  }*/

  FtempiOttimizzazione.open ("OUT_ifopt_TempiOttimizzazione.txt");
  FtempiCPU.open ("OUT_ifopt_TempiCPU.txt");
  FdurataPercorso.open ("OUT_ifopt_DurataPercorso.txt");
  FdistanzaPercorsa.open ("OUT_ifopt_DistanzaPercorsa.txt");
  

  ros::init(argc, argv, "mbnodecontrollerifopt");
  ros::NodeHandle node;
  if (!ros::master::check())
    return (0);

  Controller controller(node);

  //ros::Subscriber subClock = node.subscribe("/clock", 1, &Controller::clockCallbackTEST, &controller);

  //ros::Subscriber subPioneer_pose = node.subscribe("/vrpn_client_node/epuck_robot_4/pose", 1, &Controller::ePuck_poseCallbackTEST, &controller);
  ros::Subscriber subPioneer_pose = node.subscribe("/ePuck2/pose", 1, &Controller::ePuck_poseCallbackTEST, &controller);
  ros::Subscriber subTarget = node.subscribe("/target", 1, &Controller::targetCallback, &controller);
  ros::Subscriber subTrajectory = node.subscribe("/trajectory", 1, &Controller::trajectoryCallback, &controller);
  
  ros::Timer timer1 = node.createTimer(ros::Duration(0.1), &Controller::timer1CallbackTEST, &controller);

	// 10 Hz
	ros::Rate loop_rate(10); 
  ros::spin();

  FtempiOttimizzazione.close();
  FtempiCPU.close();
  FdurataPercorso.close();
  FdistanzaPercorsa.close();

  return 0;
}





